function init() {
	// Get all decks
	const decks = document.querySelectorAll('.ptcg-deck');
	decks.forEach(deck => {
		// Get all the card labels in the deck
		cards = deck.querySelectorAll('.ptcg-label');
		cards.forEach(cardLabel => {
			cardLabel.addEventListener('click', () => {
				if (cardLabel.classList.contains('selected')) {
					return;
				}

				const cardId = cardLabel.id;
				const prevCardLabel = deck.querySelector('.ptcg-label.selected');
				const prevCardData = deck.querySelector('.ptcg-tab-content.selected');
				const cardData = deck.querySelector(`#tab-${cardId}`);

				prevCardLabel.classList.remove('selected');
				prevCardData.classList.remove('selected');
				prevCardData.ariaHidden = true;

				cardLabel.classList.add('selected');
				cardData.classList.add('selected');
				cardData.ariaHidden = false;
			});
		});
	});
}

init();
