vim.filetype.add({ extension = { html = "htmldjango" } })
for _, b in pairs(vim.api.nvim_list_bufs()) do
	if vim.api.nvim_get_option_value("filetype", { buf = b }) == "html" then
		vim.api.nvim_set_option_value("filetype", "htmldjango", { buf = b })
	end
end
